package eg.edu.alexu.csd.filestructure.sort;

import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;

public class HeapImpl<T extends Comparable<T>> implements IHeap<T> {

	private ArrayList<INode> heap;
	private int size = 0;

	public HeapImpl() {
		heap = new ArrayList<INode>();
	}

	@Override
	public INode<T> getRoot() {
		// TODO Auto-generated method stub
		if (size() > 0) {
			return heap.get(0);
		} else
			return null;

	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public void heapify(INode<T> node) {
		// TODO Auto-generated method stub

		if (size() == 0 || size() == 1)
			return;
		INode left = node.getLeftChild();
		INode right = node.getRightChild();
		INode largest;

		if (left != null && left.getValue().compareTo(node.getValue()) > 0)
			largest = left;
		else
			largest = node;
		if (right != null && right.getValue().compareTo(largest.getValue()) > 0)
			largest = right;
		if (largest.getValue().compareTo(node.getValue()) != 0) {
			swap(node, largest);
			heapify(largest);
		}
	}

	@Override
	public T extract() {
		// TODO Auto-generated method stub
		T max = null;
		if (size() > 0) {
			max = getRoot().getValue();
			swap(heap.get(0), heap.get(size() - 1));
			heap.remove(size() - 1);
			size--;
			heapify(getRoot());
		}
		return max;

	}

	@Override
	public void insert(T element) {
		// TODO Auto-generated method stub
		if (element != null) {
			if (size() == 0) {
				INode n = new Node(0);
				n.setValue(element);
				heap.add(n);
				size++;
			} else {
				INode n = new Node(size());
				n.setValue(element);
				heap.add(n);
				bubbleUp(n);
				size++;
			}
		}

	}

	@Override
	public void build(Collection<T> unordered) {
		// TODO Auto-generated method stub
		for (Iterator iterator = unordered.iterator(); iterator.hasNext();) {

			insert((T) iterator.next());
		}

		for (int i = (int) Math.floor(size() / 2); i > 1; i--) {
			heapify(heap.get(i));
		}

	}

	public void swap(INode<T> a, INode<T> b) {
		T temp = a.getValue();
		a.setValue(b.getValue());
		b.setValue(temp);
	}

	public ArrayList<INode> getHeap() {
		return heap;
	}

	private void bubbleUp(INode n) {
		INode parent = n.getParent();
		while (parent != null && parent.getValue().compareTo(n.getValue()) < 0) {
			swap(parent, n);
			n = parent;
			parent = n.getParent();
		}
	}

	public void heapSort() {
		int size2 = heap.size();
		for (int i = heap.size() - 1; i >= 1; i--) {
			swap((INode) heap.get(0), (INode) (heap).get(i));
			size--;
			heapify((INode)heap.get(0));
		}
      size = size2;
	}

	private class Node<T extends Comparable<T>> implements INode<T> {

		private int index;
		private T value;

		public Node(int i) {
			this.index = i;
		}

		@Override
		public INode<T> getLeftChild() {
			// TODO Auto-generated method stub
			if ((2 * index + 1) <= size() - 1) {
				return heap.get(2 * index + 1);
			}
			return null;
		}

		@Override
		public INode<T> getRightChild() {
			// TODO Auto-generated method stub
			if ((2 * index + 2) <= size() - 1) {
				return heap.get(2 * index + 2);
			}
			return null;
		}

		@Override
		public INode<T> getParent() {
			// TODO Auto-generated method stub
			if (index != 0) {
				return heap.get((int) Math.floor((index - 1) / 2));
			}
			return null;
		}

		@Override
		public T getValue() {
			// TODO Auto-generated method stub
			return value;
		}

		@Override
		public void setValue(T value) {
			// TODO Auto-generated method stub
			this.value = value;

		}

	}

}
