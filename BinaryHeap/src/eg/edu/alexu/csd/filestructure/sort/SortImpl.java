package eg.edu.alexu.csd.filestructure.sort;

import java.util.Collections;

public class SortImpl<T extends Comparable<T>> implements ISort<T> {

	@Override
	public IHeap heapSort(java.util.ArrayList<T> unordered) {
		// TODO Auto-generated method stub

		HeapImpl heap = new HeapImpl();
		heap.build(unordered);
		heap.heapSort();

		return heap;
	}

	/* Bubble sort */
	@Override
	public void sortSlow(java.util.ArrayList<T> unordered) {
		// TODO Auto-generated method stub
		int j;
		boolean flag = true;
		while (flag) {
			flag = false;
			for (j = 0; j < unordered.size() - 1; j++) {
				if (unordered.get(j).compareTo(unordered.get(j + 1)) > 0) {
					Collections.swap(unordered, j, j + 1);
					flag = true;
				}
			}
		}
	}

	/* Quick sort */
	@Override
	public void sortFast(java.util.ArrayList<T> unordered) {
		// TODO Auto-generated method stub
		quickSort(unordered, 0 , unordered.size()-1);

	}

	private void quickSort(java.util.ArrayList<T> A, int p, int r) {

		int q;
		if(p < r){
		q= partition(A, p, r);
		quickSort(A, p, q-1);
		quickSort(A, q + 1, r);
		}
	}

	private int partition(java.util.ArrayList<T> A, int p, int r) {

		T x = A.get(r);
		int i = p - 1;
		for (int j = p; j <= r - 1; j++) {
			if (A.get(j).compareTo(x) <= 0) {
				i++;
				Collections.swap(A, i, j);

			}
		}
		Collections.swap(A, i + 1, r);
		return i + 1;

	}

}
