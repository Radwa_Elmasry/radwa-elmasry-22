package eg.edu.alexu.csd.filestructure.hash;

import java.util.ArrayList;

public class QuadraticProbing<k, v> implements IHash<k, v>, IHashQuadraticProbing {

	private Pair<k, v>[] hashTable = new Pair[1200];
	private int collisions = 0;
	private int size = 0;

	@Override
	public void put(k key, v value) {
		// TODO Auto-generated method stub

		int h1 = key.hashCode() % capacity();
		int hash = h1;
		boolean flag = false;
		int i = 0;
		while (hashTable[hash] != null && hashTable[hash].getKey() != key) {
			i++;
			hash = (h1 + i*i) % capacity();
			collisions++;
			flag = true;
			if (i == capacity()) {
				collisions++;
				resize(2 * hashTable.length);
				put(key,value);
				return;
			}
		}
		if (flag)
			collisions++;

		hashTable[hash] = new Pair<k, v>(key, value);

		size++;
		if (size == capacity()) {
			collisions++;
			collisions += capacity();
			resize(2 * hashTable.length);
		}

	}

	@Override
	public String get(k key) {
		// TODO Auto-generated method stub
		if (key != null) {
			if (contains(key)) {
				for (int i = 0; i < hashTable.length; i++) {
					if (hashTable[i] != null && hashTable[i].getKey().equals(key)) {
						return (String) hashTable[i].getValue();
					}
				}

			}
		}
		return null;
	}

	@Override
	public void delete(k key) {
		// TODO Auto-generated method stub
		if (key != null && size > 0) {
			// if (contains(key)) {

			for (int i = 0; i < hashTable.length; i++) {

				if (hashTable[i] != null && hashTable[i].getKey().equals(key)) {
					System.out.println(hashTable[i].getKey());
					hashTable[i] = null;
					size--;
				}
			}
			// }
		}
	}

	@Override
	public boolean contains(k key) {
		// TODO Auto-generated method stub
		if (key != null && size > 0) {
			for (int i = 0; i < hashTable.length; i++) {
				if (hashTable[i] != null && hashTable[i].getKey().equals(key)) {
					return true;
				}
			}
		}

		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		for (int i = 0; i < hashTable.length; i++) {
			if (hashTable[i] != null)
				return false;
		}
		return true;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public int capacity() {
		// TODO Auto-generated method stub
		return hashTable.length;
	}

	@Override
	public int collisions() {
		// TODO Auto-generated method stub
		return collisions;
	}

	@Override
	public Iterable<k> keys() {
		// TODO Auto-generated method stub
		ArrayList<k> keys = new ArrayList<k>();
		for (int i = 0; i < hashTable.length; i++) {
			if (hashTable[i] != null)
				keys.add(hashTable[i].getKey());
		}
		return keys;
	}

	private void resize(int capacity) {
		size = 0;

		Pair<k, v>[] temp = hashTable;

		hashTable = new Pair[capacity];
		for (int i = 0; i < temp.length; i++) {
			if (temp[i] != null) {
				put(temp[i].getKey(), temp[i].getValue());
			}
		}

	}

}
