package eg.edu.alexu.csd.filestructure.hash;

import java.util.ArrayList;

public class Chaining<k, v> implements IHash<k, v>, IHashChaining {

	private int SIZE = 1200;
	private ArrayList<Pair<k, v>>[] hashTable = new ArrayList[SIZE];
	ArrayList<k> keys = new ArrayList<k>();
	private int collisions = 0;
	private int size = 0;

	@Override
	public void put(k key, v value) {
		// TODO Auto-generated method stub
		boolean temp = false;
		if (hashTable[key.hashCode() % SIZE] != null) {

			if (key != null) {
				ArrayList<Pair<k, v>> oldList = hashTable[key.hashCode() % SIZE];
				collisions += oldList.size();
				for (int i = 0; i < oldList.size(); i++) {
					if (oldList.get(i).getKey() == key) {
						oldList.set(i, new Pair<k, v>(key, value));
						temp = true;
					}
				}
				if (!temp) {
					oldList.add(new Pair<k, v>(key, value));
					keys.add(key);

				}
				hashTable[key.hashCode() % SIZE] = oldList;
			}
		} else {

			if (key != null) {
				ArrayList<Pair<k, v>> newList = new ArrayList<Pair<k, v>>();
				newList.add(new Pair<k, v>(key, value));
				hashTable[key.hashCode() % SIZE] = newList;
				keys.add(key);
			}
		}
		size++;
	}

	@Override
	public String get(k key) {
		// TODO Auto-generated method stub
		if (key != null) {
			if (contains(key)) {
				return (String) hashTable[key.hashCode() % SIZE].get(0).getValue();
			}
		}
		return null;
	}

	@Override
	public void delete(k key) {
		// TODO Auto-generated method stub
		if (key != null && size > 0) {
			if (contains(key)) {
				for (int i = 0; i < hashTable.length; i++) {
					for (int j = 0; hashTable[i] != null && j < hashTable[i].size(); j++) {
						if (hashTable[i].get(j).getKey() == key) {
							hashTable[i].set(j, null);
						}
					}
				}
				keys.remove(key);
				size--;
			}
		}
	}

	@Override
	public boolean contains(k key) {
		// TODO Auto-generated method stub
		if (key != null && size > 0) {
			if (hashTable[key.hashCode() % SIZE] != null)
				return true;
		}

		return false;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub

		if (size == 0)
			return true;
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return size;
	}

	@Override
	public int capacity() {
		// TODO Auto-generated method stub
		return hashTable.length;
	}

	@Override
	public int collisions() {
		// TODO Auto-generated method stub
		return collisions;
	}

	@Override
	public Iterable<k> keys() {
		// TODO Auto-generated method stub
		return keys;
	}

}
