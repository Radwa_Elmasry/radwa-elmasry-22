package eg.edu.alexu.csd.filestructure.graphs;

import java.awt.Point;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;

public class Graph implements IGraph {
	private static int INFINITY = Integer.MAX_VALUE / 2;
	private Map<Integer, ArrayList<Point>> adjacencyList = new HashMap<Integer, ArrayList<Point>>();
	private int adjacencyMatrix [][];
	private int noOfVertices = 0;
	private int noOfEdges = 0;
	private ArrayList<Integer> dijkstraProcess = new ArrayList<Integer>();
	private ArrayList<Integer> vertices = new ArrayList<Integer>();

	@Override
	public void readGraph(File file) {

		if (file != null && file.isFile()) {

			try (BufferedReader br = new BufferedReader(new FileReader(file))) {
				String sCurrentLine = br.readLine();
				if (sCurrentLine != null) {
					String[] line = sCurrentLine.split(" ");
					if (line[0] != null && line[1] != null) {
						noOfVertices = Integer.parseInt(line[0]);
						noOfEdges = Integer.parseInt(line[1]);
						adjacencyMatrix = new int [noOfVertices][noOfVertices];
					} else {
						throw new RuntimeException();
					}
					for (int i = 0; i < noOfVertices; i++) {
						adjacencyList.put(i, new ArrayList<Point>());
					}
					int counter = 0;
					while ((sCurrentLine = br.readLine()) != null) {
						line = sCurrentLine.split(" ");
						if (line[0] != null && line[1] != null && line[2] != null) {
							int source = Integer.parseInt(line[0]);
							int destination = Integer.parseInt(line[1]);
							int weight = Integer.parseInt(line[2]);
							if (source < noOfVertices && destination < noOfVertices) {
								ArrayList<Point> slist = adjacencyList.get(source);
								slist.add(new Point(destination, weight));
								adjacencyList.put(source, slist);
								adjacencyMatrix[source][destination] = weight;
								if (!vertices.contains(source)) {
									vertices.add(source);
								}
								if (!vertices.contains(destination)) {
									vertices.add(destination);
								}
							}
							counter++;

						} else {
							throw new RuntimeException();
						}
					}

					if (counter != noOfEdges) {
						throw new RuntimeException();
					}

				}
			} catch (IOException e) {
				e.printStackTrace();
				throw new RuntimeException();
			}
		} else {
			throw new RuntimeException();
		}

	}

	@Override
	public int size() {

		return noOfEdges;
	}

	@Override
	public ArrayList<Integer> getVertices() {

		/*
		 * ArrayList<Integer> vertices = new ArrayList<Integer>(); for (int i =
		 * 0; i < noOfVertices; i++) { vertices.add(i); }
		 */
		return vertices;
	}

	@Override
	public ArrayList<Integer> getNeighbors(int v) {
	
		//ArrayList<Point> adj = adjacencyList.get(v);
		ArrayList<Integer> list = new ArrayList<Integer>();
		for (int i = 0; i <  adjacencyList.get(v).size(); i++) {
			list.add( adjacencyList.get(v).get(i).x);
		}

		return list;
	}

	int minDistance(int dist[], Boolean sptSet[]) {
		int min = INFINITY, min_index = -1;

		for (int v = 0; v < dist.length; v++)
			if (sptSet[v] == false && dist[v] <= min) {
				min = dist[v];
				min_index = v;
			}

		return min_index;
	}

	/*@Override
	public void runDijkstra(int src, int[] distances) {

		int V = getVertices().size();
		Boolean sptSet[] = new Boolean[distances.length];
		for (int i = 0; i < distances.length; i++) {
			distances[i] = INFINITY;
			sptSet[i] = false;
		}

		distances[src] = 0;

		for (int count = 0; count < V - 1; count++) {
			int u = minDistance(distances, sptSet);

			sptSet[u] = true;

			dijkstraProcess.add(u);
			for (int v = 0; v < getVertices().size(); v++) {

				if (v<adjacencyList.get(u).size() &&!sptSet[v] && distances[u] != INFINITY
						&& distances[u] + adjacencyList.get(u).get(v).y < distances[v]) {
					distances[v] = distances[u] + adjacencyList.get(u).get(v).y;
				}
			}
		}

	}*/
	

	@Override
	public void runDijkstra(int src, int[] distances) {
	        Boolean sptSet[] = new Boolean[distances.length];
	 
	        int V = getVertices().size();
	    	for (int i = 0; i < distances.length; i++) {
				distances[i] = INFINITY;
				sptSet[i] = false;
			}

			distances[src] = 0;
	 
	        // Find shortest path for all vertices
	        for (int count = 0; count < V-1; count++)
	        {
	            int u = minDistance(distances, sptSet);
	 
	            sptSet[u] = true;

	            for (int v = 0; v < V; v++)

	                if (!sptSet[v] && adjacencyMatrix[u][v]!=0 &&
	                        distances[u] != Integer.MAX_VALUE &&
	                        distances[u]+adjacencyMatrix[u][v] < distances[v])
	                    distances[v] = distances[u] + adjacencyMatrix[u][v];
	        }
	    }

	@Override
	public ArrayList<Integer> getDijkstraProcessedOrder() {

		return dijkstraProcess;
	}

	@Override
	public boolean runBellmanFord(int src, int[] distances) {

		int V = getVertices().size();

		for (int i = 0; i < distances.length; i++) {
			distances[i] = INFINITY;
		}

		distances[src] = 0;

		for (int i = 0; i < V - 1; i++) {
			for (int j = 0; j < adjacencyList.size(); j++) {
				for (int j2 = 0; j2 < adjacencyList.get(j).size(); j2++) {
					int v = adjacencyList.get(j).get(j2).x;
					int egdeWeight = adjacencyList.get(j).get(j2).y;
					if (distances[j] + egdeWeight < distances[v]) {
						distances[v] = distances[j] + egdeWeight;
					}
				}
			}
		}

		for (int j = 0; j < adjacencyList.size(); j++) {
			for (int j2 = 0; j2 <  adjacencyList.get(j).size(); j2++) {
				int v = adjacencyList.get(j).get(j2).x;
				int egdeWeight = adjacencyList.get(j).get(j2).y;
				if (distances[j] + egdeWeight < distances[v]) {
					return false;
				}
			}
		}

		return true;
	}

}
