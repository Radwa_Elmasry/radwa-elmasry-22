package eg.edu.alexu.csd.filestructure.avl;

public class AVLTree<T extends Comparable<T>> implements IAVLTree<T> {

	private static final int ALLOWED_IMBALANCE = 1;
	private Node<T> root;

	@Override
	public void insert(T key) {
		if (key != null) {
			root = insertNode(key, root);
		}
	}

	private Node<T> insertNode(T x, Node<T> t) {

		if (t == null) {
			return new Node<T>(x, null, null);
		} else {

			int compareResult = x.compareTo(t.getValue());
			if (compareResult < 0) {
				t.setLeftNode(insertNode(x, (Node<T>) t.getLeftChild()));
			} else if (compareResult >= 0) {
				t.setRightNode(insertNode(x, (Node<T>) t.getRightChild()));
			}
		}
		return balance(t);

	}

	private Node<T> balance(Node<T> t) {
		if (t == null)
			return t;

		if (height((Node<T>) t.getLeftChild()) - height((Node<T>) t.getRightChild()) > ALLOWED_IMBALANCE)
			if (height((Node<T>) ((Node<T>) t.getLeftChild()).getLeftChild()) >= height(
					(Node<T>) ((Node<T>) t.getLeftChild()).getRightChild()))
				t = rotateWithLeftChild(t);
			else
				t = doubleWithLeftChild(t);
		else if (height((Node<T>) t.getRightChild()) - height((Node<T>) t.getLeftChild()) > ALLOWED_IMBALANCE)
			if (height((Node<T>) ((Node<T>) t.getRightChild()).getRightChild()) >= height(
					(Node<T>) ((Node<T>) t.getRightChild()).getLeftChild()))
				t = rotateWithRightChild(t);
			else
				t = doubleWithRightChild(t);

		t.setHeight(Math.max(height((Node<T>) t.getLeftChild()), height((Node<T>) t.getRightChild())) + 1);

		return t;
	}

	private Node<T> doubleWithRightChild(Node<T> k3) {
		k3.setRightNode(rotateWithLeftChild((Node<T>) k3.getRightChild()));
		return rotateWithRightChild(k3);
	}

	private Node<T> rotateWithRightChild(Node<T> k2) {
		Node<T> k1 = (Node<T>) k2.getRightChild();
		k2.setRightNode(k1.getLeftChild());
		k1.setLeftNode(k2);
		k2.setHeight(Math.max(height((Node<T>) k2.getLeftChild()), height((Node<T>) k2.getRightChild())) + 1);
		k1.setHeight(Math.max(height((Node<T>) k1.getRightChild()), height(k2)) + 1);
		return k1;
	}

	private Node<T> doubleWithLeftChild(Node<T> k3) {
		k3.setLeftNode(rotateWithRightChild((Node<T>) k3.getLeftChild()));
		return rotateWithLeftChild(k3);
	}

	private Node<T> rotateWithLeftChild(Node<T> k2) {
		Node<T> k1 = (Node<T>) k2.getLeftChild();
		k2.setLeftNode(k1.getRightChild());
		k1.setRightNode(k2);
		k2.setHeight(Math.max(height((Node<T>) k2.getLeftChild()), height((Node<T>) k2.getRightChild())) + 1);
		k1.setHeight(Math.max(height((Node<T>) k1.getLeftChild()), height(k2)) + 1);
		return k1;
	}

	@Override
	public boolean delete(T key) {

		if (search(key)) {
			root = deleteNode(key, root);
			return true;
		}
		return false;
	}

	private Node<T> deleteNode(T x, Node<T> t) {
		if (t == null)
			return t; // Item not found; do nothing

		int compareResult = x.compareTo((T) t.getValue());

		if (compareResult < 0)
			t.setLeftNode(deleteNode(x, (Node<T>) t.getLeftChild()));
		else if (compareResult > 0)
			t.setRightNode(deleteNode(x, (Node<T>) t.getRightChild()));
		else if (t.getLeftChild() != null && t.getRightChild() != null) // Two
																		// children
		{
			t.setValue(findMin((Node<T>) t.getRightChild()).getValue());
			t.setRightNode(deleteNode((T) t.getValue(), (Node<T>) t.getRightChild()));
		} else
			t = (t.getLeftChild() != null) ? (Node<T>) t.getLeftChild() : (Node<T>) t.getRightChild();
		return balance(t);
	}

	private Node<T> findMin(Node<T> t) {
		if (t == null)
			return null;
		else if (t.getLeftChild() == null)
			return t;
		return findMin((Node<T>) t.getLeftChild());
	}

	@Override
	public boolean search(T key) {
		if (key != null) {
			return searchNode(root, key);
		}
		return false;
	}

	private boolean searchNode(Node<T> r, T val) {
		boolean found = false;
		while ((r != null) && !found) {
			T rval = r.getValue();
			if (val.compareTo(rval) < 0)
				r = (Node<T>) r.getLeftChild();
			else if (val.compareTo(rval) > 0)
				r = (Node<T>) r.getRightChild();
			else {
				found = true;
				break;
			}
			found = searchNode(r, val);
		}
		return found;
	}

	@Override
	public int height() {
		// TODO Auto-generated method stub
		if (root != null) {
			return height(root) + 1;
		}
		return 0;
	}

	private int height(Node<T> node) {
		return node == null ? -1 : node.getHeight();
	}

	@Override
	public INode<T> getTree() {

		return root;
	}

	@SuppressWarnings("hiding")
	public class Node<T extends Comparable<T>> implements INode<T> {

		INode<T> left, right;
		T value;
		int height;

		Node(T element) {
			this(element, null, null);
		}

		Node(T element, INode<T> left, INode<T> right) {
			this.value = element;
			this.left = left;
			this.right = right;
			height = 0;
		}

		@Override
		public INode<T> getLeftChild() {
			return left;
		}

		@Override
		public INode<T> getRightChild() {
			return right;
		}

		@Override
		public T getValue() {
			return value;
		}

		@Override
		public void setValue(T value) {
			this.value = value;
		}

		public void setLeftNode(INode<T> newLeft) {
			this.left = newLeft;
		}

		public void setRightNode(INode<T> newRight) {
			this.right = newRight;
		}

		public int getHeight() {
			return height;
		}

		public void setHeight(int height) {
			this.height = height;
		}

	}

}
