package eg.edu.alexu.csd.filestructure.avl;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;

public class Dictionary implements IDictionary {

	private AVLTree<String> avl = new AVLTree<String>();
	private int size = 0;

	@Override
	public void load(File file) {

		try (BufferedReader br = new BufferedReader(new FileReader(file))) {
			String sCurrentLine;
			while ((sCurrentLine = br.readLine()) != null) {
				insert(sCurrentLine);
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@Override
	public boolean insert(String word) {

		if (!avl.search(word)) {
			avl.insert(word);
			size++;
			return true;
		}

		return false;
	}

	@Override
	public boolean exists(String word) {

		return avl.search(word);
	}

	@Override
	public boolean delete(String word) {
		if (avl.delete(word)) {
			size--;
			return true;
		}
		return false;
	}

	@Override
	public int size() {
		return size;
	}

	@Override
	public int height() {

		return avl.height();
	}

}
